// ==UserScript==
// @name        @nifty search customizer
// @namespace   https://javelin.works/
// @version     0.2.0
// @description @nifty検索結果をカスタマイズ
// @author      sanadan <jecy00@gmail.com>
// @match       https://search.nifty.com/*
// @license     MIT
// ==/UserScript==

(function () {
  'use strict'

  document.head.insertAdjacentHTML('beforeend', `
<style>
.lstUnit, #nifsrvul {
  display: none;
}
a:visited h3 {
  color: green;
}
</style>
`
  )
})()
